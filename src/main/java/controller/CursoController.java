package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.CursoService;
import domain.Curso;

@Controller
public class CursoController {

    @Autowired
    CursoService cursoService;

    @RequestMapping(value = "/curso", method = RequestMethod.POST)
    String saveCurso(@ModelAttribute Curso curso, ModelMap model) {
        System.out.println("savving: " + curso.getId());
        cursoService.save(curso);
        return showCurso(curso.getId(), model);
    }
    @RequestMapping(value = "/add-curso", method = RequestMethod.GET)
    String addNewCurso(@RequestParam(required = false) Long id, ModelMap model) {
        Curso curso = id == null ? new Curso() : cursoService.get(id);
        model.addAttribute("curso", curso);
        return "add-curso";
    }

    @RequestMapping(value = "/curso", method = RequestMethod.GET)
    String showCurso(@RequestParam(required = false) Long curso_id, ModelMap model) {
        if (curso_id != null) {
            Curso curso = cursoService.get(curso_id);
            model.addAttribute("curso", curso);
            return "curso";
        } else {
            Collection<Curso> cursos = cursoService.getAll();
            model.addAttribute("cursos", cursos);
            return "cursos";
        }
    }
    @RequestMapping(value = "/find-curso", method = RequestMethod.GET)
    String findCursoI(String Codigo, ModelMap model) {
        return "find-curso";
    }
    @RequestMapping(value = "/find-curso", method = RequestMethod.POST)
    String findCursoII(String codigo, ModelMap model) {
        System.err.println(codigo);
        Curso curso= cursoService.buscaCurso(codigo);
        model.addAttribute("curso", curso);
        return "alumno";

    }
}
