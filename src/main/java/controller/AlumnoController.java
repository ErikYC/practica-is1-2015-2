package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.AlumnoService;
import domain.Alumno;

@Controller
public class AlumnoController {

    @Autowired
    AlumnoService alumnoService;

    @RequestMapping(value = "/alumno", method = RequestMethod.POST)
    String saveAlumno(@ModelAttribute Alumno alumno, ModelMap model) {
        System.out.println("savving: " + alumno.getId());
        alumnoService.save(alumno);
        return showCurso(alumno.getId(), model);
    }
    @RequestMapping(value = "/add-alumno", method = RequestMethod.GET)
    String addNewAlumno(@RequestParam(required = false) Long id, ModelMap model) {
        Alumno alumno = id == null ? new Alumno() : alumnoService.get(id);
        model.addAttribute("alumno", alumno);
        return "add-alumno";
    }

    @RequestMapping(value = "/alumno", method = RequestMethod.GET)
    String showCurso(@RequestParam(required = false) Long id, ModelMap model) {
        if (id != null) {
            Alumno alumno = alumnoService.get(id);
            model.addAttribute("alumno", alumno);
            return "alumno";
        } else {
            Collection<Alumno> alumnado = alumnoService.getAll();
            model.addAttribute("alumnado", alumnado);
            return "alumnado";
        }
    }
    @RequestMapping(value = "/find-alumno", method = RequestMethod.GET)
    String findAlumnoI(String apellidoPaterno, ModelMap model) {
        	return "find-alumno";
        }
    @RequestMapping(value = "/search-alumno", method = RequestMethod.POST)
    String findAlumnoII(String apellidoPaterno, ModelMap model) {
        System.err.println(apellidoPaterno);
        Alumno alumno= alumnoService.buscaApellido(apellidoPaterno);
        model.addAttribute("alumno", alumno);
        return "alumno";

    }
}
