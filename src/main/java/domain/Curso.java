package domain;

import javax.persistence.*;
import java.util.List;
import java.util.Collection;

@Entity
@Table(name="Curso")
@NamedQuery(name="Curso.findAll", query="SELECT p FROM Curso	 p")
public class Curso implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "curso_id_generator", sequenceName = "curso_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "curso_id_generator")
	private Long curso_id;

	private String codigo;

	private String nombre;

	private Integer credito;

	@Column(length = 64)
	private List<Curso> prerequisitos;

	@OneToMany(mappedBy = "curso", fetch = FetchType.LAZY)
		private List<Matricula> CursoMatricula;

	@Override
	public Long getId() {
		return curso_id;
	}

	@Override
	public void setId(Long id) {
		this.curso_id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCreditos() {
		return credito;
	}

	public void setCreditos(Integer creditos) {
		this.credito = creditos;
	}

}
