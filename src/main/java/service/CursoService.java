package service;

import java.util.Collection;

import domain.Curso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.CursoRepository;
import repository.PersonRepository;
import domain.Person;

@Service
public class CursoService {

    @Autowired
    CursoRepository cursoRepository;

    @Transactional
    public void save(Curso curso) {
        if (curso.getId() == null) {
            cursoRepository.persist(curso);
        } else {
            cursoRepository.merge(curso);
        }
    }

    public Curso get(Long id) {
        return cursoRepository.find(id);
    }

    public Collection<Curso> getAll() {
        return cursoRepository.findAll();
    }

    public Curso buscaCurso(String alumno){ return cursoRepository.buscarCurso(alumno); }
}