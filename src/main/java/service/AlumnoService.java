package service;

import java.util.Collection;

import domain.Alumno;
import domain.Curso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.AlumnoRepository;
import repository.CursoRepository;
import repository.PersonRepository;
import domain.Person;

@Service
public class AlumnoService {

    @Autowired
    AlumnoRepository alumnoRepository;

    @Transactional
    public void save(Alumno alumno) {
        if (alumno.getId() == null) {
            alumnoRepository.persist(alumno);
        } else {
            alumnoRepository.merge(alumno);
        }
    }

    public Alumno get(Long alumno_id) {
        return alumnoRepository.find(alumno_id);
    }

    public Collection<Alumno> getAll() {
        return alumnoRepository.findAll();
    }

    public Alumno buscaApellido(String alumno){ return alumnoRepository.buscarApellido(alumno); }
}