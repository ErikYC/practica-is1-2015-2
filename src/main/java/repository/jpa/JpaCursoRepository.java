package repository.jpa;

import domain.Curso;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;

import repository.CursoRepository;
import domain.Curso;

@Repository
public class JpaCursoRepository extends JpaBaseRepository<Curso, Long> implements
        CursoRepository {
    @Override
    public Curso buscarCurso(String codigo) {
        String jpaQuery = "SELECT p FROM Curso p WHERE p.codigo = :codigo";
        TypedQuery<Curso> query = entityManager.createQuery(jpaQuery, Curso.class);
        query.setParameter("codigo", codigo);
        return getFirstResult(query);
    }
}