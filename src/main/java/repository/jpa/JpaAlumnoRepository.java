package repository.jpa;

import domain.Alumno;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;

import repository.AlumnoRepository;

@Repository
public class JpaAlumnoRepository extends JpaBaseRepository<Alumno, Long> implements
        AlumnoRepository {
    @Override
    public Alumno buscarApellido(String alumno) {
                String jpaQuery = "SELECT p FROM Alumno p WHERE p.apellidoPaterno = :apellido";
        		TypedQuery<Alumno> query = entityManager.createQuery(jpaQuery, Alumno.class);
        		query.setParameter("apellido", alumno);
        		return getFirstResult(query);
    }
}