package repository;

import domain.Alumno;

public interface AlumnoRepository extends BaseRepository<Alumno, Long> {
         Alumno buscarApellido(String alumno);
}